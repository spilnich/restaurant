package com.javarush.task.task27.task2712.comparators;

import com.javarush.task.task27.task2712.ad.Advertisement;

import java.util.Comparator;

public class AdvertisementComparatorByAmount implements Comparator<Advertisement> {
    @Override
    public int compare(Advertisement o1, Advertisement o2) {
        return (int) (o1.getAmountPerOneDisplaying() - o2.getAmountPerOneDisplaying());
    }
}
