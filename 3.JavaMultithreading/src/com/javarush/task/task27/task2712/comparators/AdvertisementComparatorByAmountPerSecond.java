package com.javarush.task.task27.task2712.comparators;

import com.javarush.task.task27.task2712.ad.Advertisement;

import java.util.Comparator;

public class AdvertisementComparatorByAmountPerSecond implements Comparator<Advertisement> {
    @Override
    public int compare(Advertisement o1, Advertisement o2) {
        return (int) (o1.getAmountPerOneDisplaying() / o1.getDuration() - o2.getAmountPerOneDisplaying() / o2.getDuration());
    }
}
