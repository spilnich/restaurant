package com.javarush.task.task27.task2712.kitchen;

import com.javarush.task.task27.task2712.ConsoleHelper;
import com.javarush.task.task27.task2712.Tablet;

import java.io.IOException;
import java.util.List;

public class Order {
    private final Tablet tablet;
    protected List<Dish> dishes = ConsoleHelper.getAllDishesForOrder();

    public Order(Tablet tablet) throws IOException {
        this.tablet = tablet;
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return " ";
        } else {
            return "Your order: " + dishes + " of " + tablet + ", cooking time " +
                    this.getTotalCookingTime() + "min";
        }
    }

    public int getTotalCookingTime() {
        int orderTimeSummary = 0;
        for (Dish dish : dishes) {
            orderTimeSummary += dish.getDuration();
        }
        return orderTimeSummary;
    }

    public boolean isEmpty() {
        return dishes.isEmpty();
    }
}
