package com.javarush.task.task27.task2712.ad;


import com.javarush.task.task27.task2712.ConsoleHelper;
import com.javarush.task.task27.task2712.comparators.AdvertisementComparatorByAmount;
import com.javarush.task.task27.task2712.comparators.AdvertisementComparatorByAmountPerSecond;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AdvertisementManager {
    private final AdvertisementStorage storage = AdvertisementStorage.getInstance();
    private int timeSeconds;

    public AdvertisementManager(int timeSeconds) {
        this.timeSeconds = timeSeconds;
    }

    public void processVideos() {
        if (storage.list().isEmpty()) {
            throw new NoVideoAvailableException();
        }
//        Collections.sort(storage.list(), new AdvertisementComparatorByAmount());
//        Collections.sort(storage.list(), new AdvertisementComparatorByAmountPerSecond());
//        Object someContent = new Object();
//        storage.list().add(new Advertisement(someContent, "First Video", 5000, 100, 3 * 60)); // 3 min
//        storage.list().add(new Advertisement(someContent, "Second Video", 100, 10, 15 * 60)); //15 min
//        storage.list().add(new Advertisement(someContent, "Third Video", 400, 2, 10 * 60)); //10 min
        Collections.sort(storage.list(), Comparator.comparingDouble(Advertisement::getAmountPerSecond).reversed());
        System.out.println(storage.list());

        List<Advertisement> tempList = new ArrayList<>();
        int currentDuration = 0;
        int currentVideo = 0;

        while (currentDuration <= timeSeconds && currentVideo < storage.list().size()) {
            if (currentDuration + storage.list().get(currentVideo).getDuration() < timeSeconds) {
                currentDuration += storage.list().get(currentVideo).getDuration();
                tempList.add(storage.list().get(currentVideo));
            }
            currentVideo++;
        }
        System.out.println(tempList);

        for (Advertisement advertisement : tempList) {
            ConsoleHelper.writeMessage(advertisement.getName() + " is displaying... " +
                    advertisement.getAmountPerOneDisplaying() + ", " + advertisement.getAmountPerSecond());
        }



//        for (Advertisement advertisement : storage.list()) {
//            if (advertisement.)
//        }
    }

//    public int getVideoListLessOfCookingTime(List<Advertisement> list) {
//        List<Advertisement> tempList = new ArrayList<>();
//        for (Advertisement advertisement : list) {
//            if (advertisement.getDuration() <= timeSeconds && advertisement.getHits() > 0) {
//                tempList.add(advertisement);
//            }
//        }
//        return tempList;
//    }
}
